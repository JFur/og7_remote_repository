
public class Mannschaftsleiter extends Spieler {

	
private String manName;
private int rabatt;
//
public Mannschaftsleiter(String name, String nachname, int telefonnummer, boolean jahresbeitrag, String manName,
		int rabatt) {
	super(name, nachname, telefonnummer, jahresbeitrag);
	this.manName = manName;
	this.rabatt = rabatt;
}
public String getManName() {
	return manName;
}
public void setManName(String manName) {
	this.manName = manName;
}
public int getRabatt() {
	return rabatt;
}
public void setRabatt(int rabatt) {
	this.rabatt = rabatt;
}

}
