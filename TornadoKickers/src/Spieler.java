
public class Spieler {
private String name;
private String nachname;
private int telefonnummer;
private boolean jahresbeitrag;
public Spieler(String name, String nachname, int telefonnummer, boolean jahresbeitrag) {
	super();
	this.name = name;
	this.nachname = nachname;
	this.telefonnummer = telefonnummer;
	this.jahresbeitrag = jahresbeitrag;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getNachname() {
	return nachname;
}
public void setNachname(String nachname) {
	this.nachname = nachname;
}
public int getTelefonnummer() {
	return telefonnummer;
}
public void setTelefonnummer(int telefonnummer) {
	this.telefonnummer = telefonnummer;
}
public boolean isJahresbeitrag() {
	return jahresbeitrag;
}
public void setJahresbeitrag(boolean jahresbeitrag) {
	this.jahresbeitrag = jahresbeitrag;
}

//

}
