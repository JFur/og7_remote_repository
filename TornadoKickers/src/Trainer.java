
public class Trainer {

private String name;
private String lizenzklasse;

//
public Trainer(String name, String lizenzklasse) {
	super();
	this.name = name;
	this.lizenzklasse = lizenzklasse;
	
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getLizenzklasse() {
	return lizenzklasse;
}
public void setLizenzklasse(String lizenzklasse) {
	this.lizenzklasse = lizenzklasse;
}


}
