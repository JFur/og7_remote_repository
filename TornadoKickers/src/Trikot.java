
public class Trikot extends Spieler {

	
private int trikotnummer;
private String spielposition;
//
public Trikot(String name, String nachname, int telefonnummer, boolean jahresbeitrag, int trikotnummer,
		String spielposition) {
	super(name, nachname, telefonnummer, jahresbeitrag);
	this.trikotnummer = trikotnummer;
	this.spielposition = spielposition;
}
public int getTrikotnummer() {
	return trikotnummer;
}
public void setTrikotnummer(int trikotnummer) {
	this.trikotnummer = trikotnummer;
}
public String getSpielposition() {
	return spielposition;
}
public void setSpielposition(String spielposition) {
	this.spielposition = spielposition;
}


}
