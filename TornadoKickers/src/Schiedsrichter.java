
public class Schiedsrichter {

private String name;
private int anzSpiele;
//
public Schiedsrichter(String name, int anzSpiele) {
	super();
	this.name = name;
	this.anzSpiele = anzSpiele;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAnzSpiele() {
	return anzSpiele;
}
public void setAnzSpiele(int anzSpiele) {
	this.anzSpiele = anzSpiele;
}

}
