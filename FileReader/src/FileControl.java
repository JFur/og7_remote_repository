import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileControl {

	public void dateiEinlesenUndAusgeben() throws IOException {
		FileReader fr = new FileReader("miriam.dat");
		BufferedReader br = new BufferedReader(fr);
		String s = br.readLine();
		do {
			System.out.println(s);
			s = br.readLine();
		}while (s != null);
	}
}
