import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 200, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("                  Clash of Egal");
		lblNewLabel.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 11));
		contentPane.add(lblNewLabel, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(4, 0, 0, 0));
		
		JLabel lblBild = new JLabel("New label");
		panel.add(lblBild);
		
		
		JLabel lblName = new JLabel("New label");
		panel.add(lblName);
		
		JLabel lblKosten = new JLabel("New label");
		panel.add(lblKosten);
		
		JButton btnNewButton_1 = new JButton("Kaufen");
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel.add(btnNewButton_1);
		
		JButton btnNewButton = new JButton("->");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
			Einheit e = Einheitenspeicher.next();
			lblName.setText(e.getName());
			lblKosten.setText(e.getKosten()+"");
			lblBild.setIcon(new ImageIcon(e.getBildpfad()));
		}});
		btnNewButton.setFont(new Font("Source Sans Pro Semibold", Font.BOLD, 11));
		contentPane.add(btnNewButton, BorderLayout.EAST);
		
		
	
	}

}
