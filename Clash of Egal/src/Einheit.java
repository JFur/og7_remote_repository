
public class Einheit {

	private String Name;
	private int Kosten;
	private String Bildpfad;
	
	//Aufgabe | Funktioniert? | Probleme
	// 1	  |		Ja		  |
	// 2      |     Ja        |
	// 3      |     Ja        |
	// 4      |     Ja        | Herr K. gab mir die L�sung
	// 5
	
	public Einheit(String Name, int Kosten, String Bildpfad) {
		this.Name= Name;
		this.Kosten= Kosten;
		this.Bildpfad= Bildpfad;
	}
	
	
	public void setName(String Name) {
		this.Name= Name;
	}
	
	public String getName() {
		return Name;
	}
	
	
	public void setKosten(int Kosten) {
		this.Kosten= Kosten;
	}
	
	public int getKosten() {
		return Kosten;
	}
	
	
	public void setBildpfad(String Bildpfad) {
		this.Bildpfad= Bildpfad;
	}
	
	public String getBildpfad() {
		return Bildpfad;
	}


	@Override
	public String toString() {
		return "Einheit [Name=" + Name + ", Kosten=" + Kosten + ", Bildpfad=" + Bildpfad + "]";
	}
	
	
	
}
