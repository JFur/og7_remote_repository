import java.awt.EventQueue;
import java.awt.LayoutManager;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BorderLayout extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BorderLayout frame = new BorderLayout();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BorderLayout() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new java.awt.BorderLayout(0, 0));
		contentPane.setLayout((LayoutManager) new BorderLayout());
		setContentPane(contentPane);
		
		JButton btnNewButton = new JButton("2");
		contentPane.add(btnNewButton, java.awt.BorderLayout.WEST);
		
		JButton btnNewButton_1 = new JButton("3");
		contentPane.add(btnNewButton_1, java.awt.BorderLayout.CENTER);
		
		JButton btnNewButton_2 = new JButton("1");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		contentPane.add(btnNewButton_2, java.awt.BorderLayout.NORTH);
		
		JButton btnNewButton_3 = new JButton("5");
		contentPane.add(btnNewButton_3, java.awt.BorderLayout.SOUTH);
		
		JButton btnNewButton_4 = new JButton("4");
		contentPane.add(btnNewButton_4, java.awt.BorderLayout.EAST);
	}

}
