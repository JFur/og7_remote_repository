
public class BubbleSort {

	Stopwatch t = new Stopwatch();
	
    public int[] intArr = { 16, 23, 14, 7, 21, 20, 6, 1, 17, 13, 12, 9, 3, 19 };

    public int[] sort() {
        int k;
        t.start();
        for (int i = 0; i < intArr.length - 1; i++) {
            if (intArr[i] < intArr[i + 1]) {
                continue;
            }
            k = intArr[i];
            intArr[i] = intArr[i + 1];
            intArr[i + 1] = k;
            sort();
            
            t.stop();
            System.out.println("Benötigte Zeit: "+ t.lies() + " ms.");
            t.reset();
        }
        return intArr;
        
        
    }
    
    public static void main(String[] args) {
    	BubbleSort bs = new BubbleSort();
        int[] arr = bs.sort();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(i + 1 + ": " + arr[i]);
        
            
        }
        
         
    }
    
} 
