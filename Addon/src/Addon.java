
public class Addon {
@Override
	public String toString() {
		return "Addon [name=" + name + ", preis=" + preis + ", iD_Nummer=" + iD_Nummer + ", bestand=" + bestand
				+ ", maxBestand=" + maxBestand + "]";
	}

	//Attribute
	private String name;
	private double preis;
	private int iD_Nummer;
	private int bestand;
	private int maxBestand;
	
	
//Konstruktor
	public Addon() {
		super();
	}	

	public Addon(String name, double preis, int iD_Nummer, int bestand, int maxBestand) {
	super();
	this.name = name;
	this.preis = preis;
	this.iD_Nummer = iD_Nummer;
	this.bestand = bestand;
	this.maxBestand = maxBestand;
}


//Methoden
	public void addon_Kaufen() {
	}
	
//getter //setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getiD_Nummer() {
		return iD_Nummer;
	}

	public void setiD_Nummer(int iD_Nummer) {
		this.iD_Nummer = iD_Nummer;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public int getMaxBestand() {
		return maxBestand;
	}

	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}
	
	
}
